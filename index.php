<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <a href="" class="igallery-tab">
        <img src="<?php echo get_template_directory_uri(); ?>/icon.svg" />
        <strong>iGallery</strong>
      </a>
      <h1>iGallery</h1>
      <a href="" class="igallery-basket"><i class="fa fa-shopping-basket"></i></a>
    </header>
    <div><?php echo get_search_form(); ?></div>
    <main class="grid">
      <?php
        $query_images_args = array(
          'post_type'      => 'attachment',
          'post_mime_type' => 'image',
          'post_status'    => 'inherit',
          'posts_per_page' => -1,
          'orderby'        => 'rand'
        );

        $query_images = new WP_Query($query_images_args);

        $attr = array(
          "width" => "200",
          "height" => "auto"
        );

        $images = array();
        foreach ($query_images->posts as $image) {
          $images[] = wp_get_attachment_url($image->ID);
          $meta = get_post_meta($image->ID, '_wp_attachment_metadata', true);
          echo '<div class="grid-item">';
          echo wp_get_attachment_image($image->ID, 'medium', false, $attr);
          echo '<div class="quick-links">';
          echo '<a href="javascript:void(0);" data-id="'.$image->ID.'" data-src="box-'.$image->ID.'" data-type="inline" data-fancybox="group"><i class="fa fa-search-plus"></i></a>';
          echo '<a href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>';
          echo '<a href="javascript:void(0);"><i class="fa fa-download"></i></a>';
          echo '</div>';
          echo '</div>';
        }
      ?>
    </main>
    <footer>
      <a href="" class="pagination__next"></a>
    </footer>
  </body>
</html>
