$(function() {
  var $grid = $('.grid').imagesLoaded( function() {
    var msnry = $('.grid').masonry({
      itemSelector: '.grid-item',
      columnWidth: 10,
      percentPosition: true
    });

    $('[data-fancybox]').fancybox({});

    $('.grid').infiniteScroll({
      // options
      path: '.grid',
      append: 'grid-item'
    });
  });

  $(document).foundation();
});