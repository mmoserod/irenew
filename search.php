<?php
if (have_posts()) {
  while (have_posts()) {
    the_post();
    $postOBJ = get_post();

    echo json_encode($postOBJ);
  }
}
?>