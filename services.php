<?php
/*
Template Name: services
*/

class Services
{
  public function execute()
  {
    $exclude = isset($_GET["exclude"]) ? json_decode($_GET["exclude"]) : array();

    $args = array(
      'post_type' => 'attachment',
      'post_mime_type' => 'image',
      'post_status' => 'inherit',
      'posts_per_page' => 15,
      'orderby' => 'rand',
      'post__not_in' => $exclude
    );

    $result = new WP_Query($args);

    return json_encode($result->posts);
  }
}
$services = new Services();
header('Content-Type: application/json');
echo $services->execute();